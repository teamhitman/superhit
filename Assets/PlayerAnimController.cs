﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimController : MonoBehaviour
{

    Animator anim;
    PlayerMovement player;
    int i = 1;
    int b = 0;
    Vector3 lastMouseCoordinate = Vector3.zero;
    private void Start()
    {
        anim = GetComponent<Animator>();
        player = GetComponentInParent<PlayerMovement>();
    }

    private void Update()
    {
        float move = Input.GetAxis("Horizontal");
        move += Input.GetAxis("Vertical");    
        
        if(Input.GetKey(KeyCode.Mouse1))
        {
            anim.SetBool("isIdle", false); 
        }
        else
        {
            anim.SetBool("isIdle", false);
        }

        anim.SetFloat("Move", move); 
        // Base layer is top layer
        i = anim.GetLayerIndex("Base Layer");
        b = anim.GetLayerIndex("Bottom Layer");
        if (i == 0)
        {
            if (move == 0)
            {                
                anim.SetBool("isIdle", true);
            }
            else 
            {
                anim.SetBool("isIdle", false);
            }
        }
        if (b == 1)
        {
            if(move == 0)
            {
                anim.SetBool("bottomIdle", true);
            }
            else
            {
                anim.SetBool("bottomIdle", false);
            }
        }
       
    }

}
