﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class ClonesTextUI : MonoBehaviour {


    Text clonesLeft;
    PlayerSplits player;
    private void Start()
    {
        player = FindObjectOfType<PlayerSplits>();
        clonesLeft = GetComponent<Text>();
    }

    private void Update()
    {
        clonesLeft.text =  "Clones Left: " + player.splitCount.ToString();
    }
}
