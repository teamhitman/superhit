﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePause : MonoBehaviour {

    ParticleSystem particle;
    float timer;
	// Use this for initialization
	void Start () {
        particle = GetComponent<ParticleSystem>();
        
	}

    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= .3f)
        {
            particle.Pause();
        }
    }
	
}
