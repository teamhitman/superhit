﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour {

    public float spawnTimer;
    float initialSpawnTimer;

    public GameObject car;
    public Transform spawnPoint; 
    void Start()
    {
        initialSpawnTimer = spawnTimer; 
    }
    void Update()
    {
        spawnTimer -= Time.deltaTime;
        if(spawnTimer <= 0)
        {
            Instantiate(car, spawnPoint.position, spawnPoint.rotation);
            spawnTimer = Random.Range(.5f, initialSpawnTimer);
        }
    }
}
