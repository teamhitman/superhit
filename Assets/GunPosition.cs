﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPosition : MonoBehaviour {

    public Transform idlePos;
    public Transform aimingPos;

    public bool idle; 
    Animator anim;

    private void Start()
    {
        anim = GetComponentInParent<Animator>(); 
    }

    private void Update()
    {
        idle = anim.GetBool("isIdle"); 
        if(!idle)
        {
            transform.position = Vector3.Lerp(transform.position, aimingPos.position, (1 * Time.deltaTime) * 3);
            transform.rotation = Quaternion.Slerp(transform.rotation, aimingPos.rotation, (1 * Time.deltaTime) * 3);
        }
        if(idle)
        {
            transform.position = Vector3.Lerp(transform.position, idlePos.position, (1 * Time.deltaTime) * 3);
            transform.rotation = Quaternion.Slerp(transform.rotation, idlePos.rotation, (1 * Time.deltaTime) * 3); 
        }
    }
}
