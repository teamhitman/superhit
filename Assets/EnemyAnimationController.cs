﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationController : MonoBehaviour {

    MovingEnemy enemy;
    Animator anim;

    public bool isCivilian;

    private void Start()
    {
        enemy = GetComponentInParent<MovingEnemy>();
        anim = GetComponent<Animator>();         
    }

    private void Update()
    {
        
        if (enemy.isIdle)
        {
            anim.SetBool("isIdle", true);
        }
        else if (enemy.isIdle == false) 
        {
            anim.SetBool("isIdle", false);
        }

        if(enemy.isDead)
        {
            anim.SetBool("isDead", true); 
        }
        else
        {
            anim.SetBool("isDead", false); 
        }
    }
}
