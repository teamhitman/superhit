﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowMidpoint : MonoBehaviour
{
   
 
    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z - transform.position.z));
        transform.position = mousePos;
    }

}