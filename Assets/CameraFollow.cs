﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float damping;
    public GameObject player;
    public float height;
    public float startHeight;
    public float maxDistance = 7;
    Vector3 center;
    GameObject midPoint;
    public bool isStart = true;
    public float startTimer;
    public bool followBullet;

    GameObject bulletToFollow;
    float timeScale = .2f;

    public float timer;
    bool hasKilled;
    Vector3 initialPos; 
    GameManager game;

    public bool canFollow = true;
    bool findEnemy;
    GameObject enemyToFollow;

    bool hasFound;
    Vector3 enemyPos;
    bool findCivilian;
    AudioSource m_audio;
    public AudioClip woosh;
    public AudioClip spotted;
    public AudioClip win;

    bool hasPlayed;
    bool played; 
    private void Start()
    {
        m_audio = GetComponent<AudioSource>();
        game = FindObjectOfType<GameManager>();
        midPoint = GameObject.Find("MidPoint");
        initialPos = transform.position;
        
    }
    private void Update()
    {
        Debug.Log(initialPos); 
        if (canFollow)
        {
            if (followBullet)
            {
                if (!hasKilled)
                {
                    if(!hasPlayed)
                    {
                        m_audio.PlayOneShot(woosh, .5f);
                        hasPlayed = true; 
                    }
                    Debug.Log("Following Bullet");
                    //transform.position = Vector3.Lerp(transform.position, new Vector3(bulletToFollow.transform.position.x, height, bulletToFollow.transform.position.z), bulletToFollow.transform.position.z);
                    transform.position = Vector3.Lerp(transform.position, new Vector3(bulletToFollow.transform.position.x, height, bulletToFollow.transform.position.z), (1 * Time.deltaTime) * 15);
                    Time.timeScale = .2f;


                    timer += Time.unscaledDeltaTime;
                    if (timer >= 3)
                    {
                        
                        followBullet = false;
                        canFollow = false;
                        hasPlayed = false;
                        timer = 0;
                       
                    }

                }

              

            }
            if (!followBullet && !canFollow)
            {
                Time.timeScale = 1;
                transform.position = initialPos;

                //transform.position = Vector3.Lerp(transform.position, new Vector3(initialPos.transform.position.x, 20, initialPos.transform.position.z), (1 * Time.deltaTime) * 15);

            }

            if (game.hasWon)
            {
                if (!played)
                {
                    m_audio.PlayOneShot(win, .5f);
                    played = true;
                }
                hasKilled = true;
            }
            else
            {
                hasKilled = false;
            }
            if (!followBullet)
            {
               
            }
        }

        if(findEnemy)
        {
            if (!played)
            {
                m_audio.PlayOneShot(spotted, .2f);
                played = true;
            }
            transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x, height, player.transform.position.z / 2), (1 * Time.deltaTime) * 10);
            Time.timeScale = .3f;
        }

        if(findCivilian)
        {
            if(!played)
            {
                m_audio.PlayOneShot(spotted, .2f);
                played = true; 
            }
            transform.position = Vector3.Lerp(transform.position, new Vector3(enemyToFollow.transform.position.x, height, enemyToFollow.transform.position.z / 2), (1 * Time.deltaTime) * 10);
            Time.timeScale = .3f;
        }
    }
    //void Update()
    //{
    //    if (isStart)
    //    {
    //        transform.position = new Vector3(midPoint.transform.position.x, startHeight, midPoint.transform.position.z);
    //        startTimer -= Time.deltaTime;
    //        if (startTimer <= 0)
    //        {
    //            isStart = false;
    //        }
    //    }

    //    if(!isStart)
    //    {
    //        Vector3 mousePos = Input.mousePosition;

    //        Vector3 pos = Input.mousePosition;
    //        pos.z = transform.position.y;
    //        pos = Camera.main.ScreenToWorldPoint(pos);

    //        Vector3 cursorPosition = pos;

    //        Vector3 playerPosition = player.transform.position;


    //        var distanceVariable = Vector3.Distance(cursorPosition, player.transform.position) * 0.5f;
    //        distanceVariable = Mathf.Clamp(distanceVariable, 0, maxDistance);
    //        center = player.transform.position + (player.transform.forward * distanceVariable);

    //        transform.position = Vector3.Lerp(transform.position, new Vector3(center.x, height, center.z), Time.deltaTime * damping);

    //    }
    //}

    public void FollowBullet(Transform bullet)
    {
        canFollow = true;
        followBullet = true;
        timer = 0; 
        bulletToFollow = bullet.transform.gameObject;
    }

    public void FindEnemy(Transform enemy)
    {
        

        if(!game.hasLost || !game.hasWon)
        {
            findEnemy = true;
            enemyToFollow = enemy.transform.gameObject;
            if (!hasFound)
            {
                enemyPos = enemy.transform.position;
                hasFound = true;
            }
        }
        else
        {
            enemyToFollow = enemy.transform.gameObject;
        }        
    }

    public void FindCivilian(Transform enemy)
    {
        if (!game.hasLost || !game.hasWon)
        {
            findCivilian = true;
            enemyToFollow = enemy.transform.gameObject;
        }
        else if(game.hasLost || game.hasWon)
        {
            findCivilian = false;
        }
            
    }
}
