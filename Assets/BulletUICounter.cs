﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletUICounter : MonoBehaviour {

    BulletDamage bullet;

    Text countText;

    private void Start()
    {
        countText = GetComponent<Text>();
        bullet = GetComponentInParent<BulletDamage>();
    }
    private void Update()
    {
        countText.text = bullet.i.ToString(); 
    }
}
