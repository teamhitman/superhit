﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
public class TimerSpawnUIController : MonoBehaviour {

    PlayerShooting player;
    public GameObject timerText;
    public GameObject spawnPos;
    public float timeForThisLevel;
    public bool hasSpawned; 
    private void Start()
    {
        player = FindObjectOfType<PlayerShooting>(); 
    }

    private void Update()
    {
        if(player.isOneActive && Input.GetKeyDown(KeyCode.Mouse0) && !hasSpawned)
        {
            SpawnText();
        }


    }

    void SpawnText()
    {
        GameObject spawned = Instantiate(timerText, spawnPos.transform.position, Quaternion.identity);
        spawned.transform.SetParent(spawnPos.transform);
        TimerTextUI ui = spawned.GetComponent<TimerTextUI>();
        ui.timeLeftTimer = timeForThisLevel;
        hasSpawned = true;
        //spawnedText.text = timeForThisLevel.ToString(); 
    }
}
