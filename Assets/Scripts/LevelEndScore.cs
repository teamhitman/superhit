﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndScore : MonoBehaviour {

    public int score;
    
    [Header("Modifiers")]
    public int enemyScore;
    public int multipleEnemyModifier;
    public int bounceTimesScoreModifier;
    public int scorePerSecondLeft;
    public int clonesModifier; 
    
    //GameManager needs to communicate this
    int enemiesLeft; 


    int scoreToAdd;   

    //public GameObject textToSpawn;

    PlayerSplits splits; 
    GameManager game;

    
    //High Score Table
    [Header("Score Tracking Variables")]
    public int totalBounces;

    public int totalEnemiesKilled;

    public int enemiesKilledBySame;
  
    public int clonesLeft;
    public int clonesLeftScore;
    int multipleEnemiesKilled;

    bool hasCalculated;

    SuccessPanelController winScreen;

    
    private void Awake()
    {
        //score = 0;
        winScreen = FindObjectOfType<SuccessPanelController>(); 
    }

    private void Start()
    {
        game = FindObjectOfType<GameManager>();
        splits = GameObject.Find("Player").GetComponent<PlayerSplits>();
    }

    //This is called from bullet
    public void AddScore(int bounceTimes, int enemiesKilled, Transform spawnPos)
    {      
        totalBounces += bounceTimes;
        totalEnemiesKilled += enemiesKilled;       

        //For spawning text mesh above the enemies

        //GameObject spawnedText = Instantiate(textToSpawn, spawnPos.position, Quaternion.identity);
        //TextMesh spawnedtext = spawnedText.GetComponent<TextMesh>();
        //spawnedtext.text = scoreToAdd.ToString("0"); 
       // HighScore();
    }

    private void Update()
    {
        clonesLeft = splits.splitCount;
        // HighScore();    
        if (game.hasWon || game.hasLost)
        {
            if(!hasCalculated)
            {
                HighScore();
                hasCalculated = true; 
            }
            
        }

        if(game.hasWon)
        {
            winScreen.SetScore(totalEnemiesKilled, totalBounces, enemiesKilledBySame, clonesLeftScore, score); 
        }
   
    }

    void HighScore()
    {
        //Enemy Combo
        enemiesKilledBySame = enemiesKilledBySame * multipleEnemyModifier;

        //Richochet bonus
        totalBounces = totalBounces * bounceTimesScoreModifier;

        //Clones Left
        clonesLeft = splits.splitCount;

        clonesLeftScore = clonesLeft * clonesModifier;

        //Total Enemies Killed
        totalEnemiesKilled = totalEnemiesKilled * enemyScore;

        score = (enemiesKilledBySame + totalBounces + clonesLeftScore + totalEnemiesKilled);
    }

    public void AddBounces(int bounces)
    {
        totalBounces += bounces;        
    }

    public void AddEnemyScore(int enemiesKilled)
    {
        totalEnemiesKilled += enemiesKilled;

        if (enemiesKilled == 2)
        {
            enemiesKilledBySame++;
        }
        if (enemiesKilled == 3)
        {
            enemiesKilledBySame += 2;
        }
        
        
    }
}
