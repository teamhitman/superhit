﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCursor : MonoBehaviour {

    public Texture2D cursor;
    public Vector3 positionOffset = Vector3.zero;

    private void Awake()
    {
        Cursor.SetCursor(cursor, positionOffset, CursorMode.Auto);
    }
    void Update ()
    {
        Cursor.SetCursor(cursor, positionOffset, CursorMode.Auto);
    }
}
