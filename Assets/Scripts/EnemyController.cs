﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : BaseEnemy {

    GameObject player;

    private Vector3? lastKnownPlayerPosition;

    public Transform visionPoint;

    public bool isTarget;
    GameManager gameManager;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>(); 
        player = GameObject.Find("Player");

    }

    void Update()
    {      
        Think();        
      
        //Lose Game Here
        if(lastKnownPlayerPosition.HasValue)
        {
           // Debug.Log("Hello player");
        }
    }
    

    void FixedUpdate()
    {
        Act();
    }

    void Think()
    {
        Vector3 deltaToPlayer = player.transform.position - visionPoint.position;
        float distanceToPlayer = deltaToPlayer.magnitude;
        Vector3 directionToPlayer = deltaToPlayer.normalized;


        //check if player is in range
        if (distanceToPlayer > visionDistance)
        {
            //the player is to far away
            return;
        }

        //do a quick calculatoin to see if the player is infront of us
        float dot = Vector3.Dot(visionPoint.forward, directionToPlayer);

        if (dot< 0)
        {//if player is behind, give up
            return;
        }
        //check if the player is within vision cone
        float angle = Vector3.Angle(visionPoint.forward, directionToPlayer);

        if (angle > visionAngle)
        {//the player is outside of our vision cone
            return;
        }
        //else
        //{            
            
        //        transform.LookAt(player.transform);
        //        lastKnownPlayerPosition = player.transform.position;
                   
        //}

        //perform raycast to see if anything is in the way
        RaycastHit hit;
        Debug.DrawRay(visionPoint.position, directionToPlayer, Color.red);
        if (Physics.Raycast(visionPoint.position, directionToPlayer, out hit, visionDistance))
        {           
            if (hit.collider.CompareTag("Player"))
            {
                //Do Thing where player loses
                Debug.Log("Hello Player");
                lastKnownPlayerPosition = player.transform.position;              
            }

        }
    }

    void Act()
    {
        if (lastKnownPlayerPosition.HasValue)
        {
            //we know where the player is
            //look at the player - this will also enforce that we only rotate along the y axis

            //transform.LookAt(lastKnownPlayerPosition.Value);
            transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);

            //Get the distance to the last known position
            float distanceToPosition = (lastKnownPlayerPosition.Value - transform.position).magnitude;

            if (distanceToPosition < visionDistance)
            {
                //Lose game
                gameManager.hasLost = true; 

            }
        }
    }

    public void Damage()
    {
        if(isTarget)
        {
            Debug.Log("You win");
            gameManager.hasWon = true;
        }
        else
        {
            gameManager.hasLost = true;
            Debug.Log("You lose"); 
        }
    }
    void Die()
    {
        Destroy(this.gameObject); 
    }

    void OnDrawGizmos()
    {
        
        //Gizmos.DrawFrustum(transform.position, visionAngle, visionDistance, visionDistance, 0);
        //Gizmos.DrawWireSphere(transform.position, visionDistance);
    }
}