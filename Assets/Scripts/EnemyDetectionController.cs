﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectionController : BaseEnemy
{
    public List<GameObject> enemyList = new List<GameObject>();
    private Vector3? lastKnownEnemyPosition;

    private void Start()
    {
        enemyList.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
    }

    private void Update()
    {
        FindDeadEnemies();
        //FindBrokenGlass();
    }

    void FindDeadEnemies()
    {
        GameObject deadEnemy;

        //Check each enemy in scene
        for (int i = 0; i < enemyList.Count; i++)
        {
            //Checks for the script component
            MovingEnemy me = enemyList[i].GetComponent<MovingEnemy>();
            if (me == null) { }
            else
            {
                //Check if an enemy is dead
                if (me.isDead)
                {
                    if (!me.stopChecking)
                    {
                        deadEnemy = enemyList[i];
                        GameObject newWayPoint = new GameObject();
                        lastKnownEnemyPosition = deadEnemy.transform.position;
                        newWayPoint.transform.position = (Vector3)lastKnownEnemyPosition;

                        Debug.Log(deadEnemy.name + " is dead");

                        //if there is a dead enemy then check how close each enemy is to the dead enemy
                        for (int j = 0; j < enemyList.Count; j++)
                        {
                            MovingEnemy me2 = enemyList[j].GetComponent<MovingEnemy>();
                            if (me2.isDead){}
                            else
                            {
                                //if an enemy is within range of a dead enemy
                                if (Vector3.Distance(enemyList[j].transform.position, deadEnemy.transform.position) < hiddenVisionDistance)
                                { 
                                    Debug.Log(enemyList[j].name + " is within range of dead guy");

                                    if (!me2.isTarget)
                                    {
                                        //create a new waypoint thats at the dead enemies position
                                        me2.wayPoint.Add(newWayPoint.transform);
                                        
                                    }
                                    else
                                    {
                                        //me2. run to exit
                                        GameObject exitWayPoint;
                                        exitWayPoint = GameObject.Find("ExitWayPoint");
                                        me2.wayPoint.Add(exitWayPoint.transform);
                                       
                                    }
                                    me.stopChecking = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
