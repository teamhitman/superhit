﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 
public class SuccessPanelController : MonoBehaviour {

    public Text kills;
    public Text richochetBonus;
    public Text enemyCombos;
    public Text clonesLeft;
    public Text scoreText;

    public Text bestScore;

    float highscore;

    int scoreIncreaseRate = 10; 
    float timer = 0;

    public int i = 0;
    int numToCountTo;

    int richochetCount;
    int richochetNumToCountTo;

    int enemycomboCount;
    int enemyCombotoCountTo;

    int clonesCount;
    int clonesToCountTo;

    int scoreCount;
    int scoreToCountTo;
    int leveltoload;
    private void Start()
    {
        leveltoload = Application.loadedLevel + 1; 
        if (!PlayerPrefs.HasKey("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", 100);
        }
    }

    

    void Update()
    {
        if(i < numToCountTo)
        {
            i += scoreIncreaseRate;
        }
        if(richochetCount < richochetNumToCountTo)
        {
            richochetCount += scoreIncreaseRate; 
        }
        if(enemycomboCount < enemyCombotoCountTo)
        {
            enemycomboCount += scoreIncreaseRate; 
        }
        if(clonesCount < clonesToCountTo)
        {
            clonesCount += 20;
        }

        if(scoreCount < scoreToCountTo)
        {
            scoreCount += 20; 
        }

        kills.text = i.ToString();
        richochetBonus.text = richochetCount.ToString();
        enemyCombos.text = enemycomboCount.ToString();
        clonesLeft.text = clonesCount.ToString();
        scoreText.text = scoreCount.ToString();
    }

    public void SetScore(int Kills, int richochet, int enemycomb, int clones, int score)
    {
        numToCountTo = Kills; 
        //kills.text = Kills.ToString();
        kills.text = i.ToString();

        richochetNumToCountTo = richochet;
        richochetBonus.text = richochetCount.ToString();

        enemyCombotoCountTo = enemycomb;
        enemyCombos.text = enemycomb.ToString();

        clonesToCountTo = clones;

        

        scoreToCountTo = score; 
        

        highscore = PlayerPrefs.GetInt("HighScore");

        if (score >= highscore)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }  

        //PlayerPrefs.Save();
        bestScore.text = highscore.ToString(); 
    }

	public void NextLevel()
    {
        Application.LoadLevel(leveltoload);   
    }

    public void Menu()
    {
        SceneManager.LoadScene(0); 
    }
}
