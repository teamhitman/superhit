﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour
{
    [Header("Stats")]
    public float visionAngle = 30f;

    public float visionDistance = 10f;

    public float moveSpeed = 2f;

    public float hiddenVisionAngle = 30f;

    public float hiddenVisionDistance = 100f;

    //public bool _isDead = false;
}
