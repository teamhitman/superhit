﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MovingEnemy : BaseEnemy
{
    GameObject player;

    private Vector3? lastKnownPlayerPosition;
    public Transform visionPoint;

    public bool isTarget;

    //public Transform[] wayPoint;
    public List<Transform> wayPoint = new List<Transform>();
    int i;
    bool countingUp = true;

    GameManager gameManager;

    private Vector3? lastKnownEnemyPosition;
    public float enemyViewDistance;
    public float enemyViewRadius;
    bool hasSpottedEnemy;

    public bool isDead;
    public bool isIdle;
    public bool stopChecking;
    public bool isCivilian;
    CameraFollow m_cam; 
    VisionConeController vision;
    AudioSource m_audio;
    public AudioClip alarm;
    bool hasPlayed;
    public AudioClip damage;
    bool hasplayedDamage; 

    void Start()
    {
        m_audio = GetComponent<AudioSource>();
        if (!isTarget)
        {
            alarm = null; 
        }
        
        vision = GetComponent<VisionConeController>(); 
        gameManager = FindObjectOfType<GameManager>(); 
        player = GameObject.Find("Player");
        i = 0;
        //isDead = _isDead;
        if (wayPoint.Count == 0)
        {
            isIdle = true;
        }
        else
        {
            isIdle = false; 
        }

        m_cam = FindObjectOfType<CameraFollow>(); 
        }

    void Update()
    {
        if(gameManager.hasWon || gameManager.hasLost)
        {
            m_audio.Pause(); 
        }
        Move();  
        Think();
        //FindDeadEnemies();
        if(wayPoint.Count != 0)
        {
            isIdle = false;
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            TargetRun();
        }
    }

    void Move()
    {
        if(wayPoint.Count != 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, wayPoint[i].transform.position, (moveSpeed * Time.deltaTime));

            var targetPoint = wayPoint[i].transform.position;
            var targetRotation = Quaternion.LookRotation(targetPoint - transform.position, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * moveSpeed * 2);

            if (Vector3.Distance(transform.position, wayPoint[i].transform.position) <= .2f)
            {
                if (countingUp)
                {
                    i += 1;
                }
                else if (!countingUp)
                {
                    i -= 1;
                }
            }

            if (i >= wayPoint.Count && countingUp)
            {
                i -= 1;
                countingUp = false;

            }
            if (i < 0 && !countingUp)
            {
                i += 1;
                countingUp = true;

            }
        }
       
    }

    void FixedUpdate()
    {
        Act();
    }

    void Think()
    {
        Vector3 deltaToPlayer = player.transform.position - visionPoint.position;
        float distanceToPlayer = deltaToPlayer.magnitude;
        Vector3 directionToPlayer = deltaToPlayer.normalized;

        //check if player is in range
        if (distanceToPlayer > visionDistance)
        {
            //the player is to far away
            return;
        }

        //do a quick calculatoin to see if the player is infront of us
        float dot = Vector3.Dot(visionPoint.forward, directionToPlayer);

        if (dot < 0)
        {//if player is behind, give up
            return;
        }
        //check if the player is within vision cone
        float angle = Vector3.Angle(visionPoint.forward, directionToPlayer);

        if (angle > visionAngle)
        {//the player is outside of our vision cone
            return;
        }
        //else
        //{            

        //        transform.LookAt(player.transform);
        //        lastKnownPlayerPosition = player.transform.position;

        //}

        //perform raycast to see if anything is in the way
        RaycastHit hit;
        Debug.DrawRay(visionPoint.position, directionToPlayer, Color.red);
        if (Physics.Raycast(visionPoint.position, directionToPlayer, out hit, visionDistance))
        {
            if (hit.collider.CompareTag("Player"))
            {
                //Do Thing where player loses
                Debug.Log("Hello Player");
                lastKnownPlayerPosition = player.transform.position;
            }

        }
    }

    void Act()
    {
        if (lastKnownPlayerPosition.HasValue)
        {
            //we know where the player is
            //look at the player - this will also enforce that we only rotate along the y axis

            //transform.LookAt(lastKnownPlayerPosition.Value);
            transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);

            //Get the distance to the last known position
            float distanceToPosition = (lastKnownPlayerPosition.Value - transform.position).magnitude;

            if (distanceToPosition < visionDistance)
            {
                //Lose game
                //gameManager.hasLost = true; 

            }
        }
    }

    public void Damage()
    {
        if(!hasplayedDamage)
        {
            m_audio.PlayOneShot(damage, .2f);
            hasplayedDamage = true;
        }
        if (isTarget)
        {
            
            gameObject.AddComponent<EnemyDeathAnim>();
            Destroy(GetComponent<VisionConeController>());
            GetComponent<MovingEnemy>().enabled = false;
            GameObject childTrans = transform.GetChild(0).gameObject;
            childTrans.SetActive(false);
            gameManager.hasWon = true;
            m_audio.Stop();
            isDead = true;
            
        }
        else if(!isTarget)
        {
            gameObject.AddComponent<EnemyDeathAnim>();
            Destroy(GetComponent<VisionConeController>());
            GetComponent<MovingEnemy>().enabled = false;
            GameObject childTrans = transform.GetChild(0).gameObject;
            childTrans.SetActive(false);
            isDead = true;
        }
        if(isCivilian)
        {
            m_cam.FindCivilian(gameObject.transform);
            gameManager.hasLost = true; 
        }
    }
    void Die()
    {
        Destroy(this.gameObject);
    }

    void OnDrawGizmos()
    {
        //Gizmos.DrawFrustum(transform.position, visionAngle, visionDistance, visionDistance, 0);
        //Gizmos.DrawWireSphere(transform.position, visionDistance);
    }

    public void TargetRun()
    {
        if (isTarget)
        {
            if(!hasPlayed)
            {
                m_audio.PlayOneShot(alarm, .1f);
                hasPlayed = true;
            }
                   
            GameObject exitWayPoint;
            exitWayPoint = GameObject.Find("ExitDoor");
            wayPoint.Add(exitWayPoint.transform);
        }
    }
}

