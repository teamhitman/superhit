﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleBullet : MonoBehaviour
{

    Rigidbody rb;
    public float moveSpeed;

    float timer;
    bool hashit;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(transform.forward * moveSpeed, ForceMode.Impulse);

        if (hashit)
        {
            timer += Time.deltaTime;
            if (timer >= .2f)
            {
                Application.LoadLevel(1);

            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Glass")
        {
            hashit = true;

        }
    }
}
