﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryCheck : MonoBehaviour
{
    public Transform GunPos;
    private Vector3 startPoint;
    private Vector3 direction;
    public LineRenderer lr;
    public int maxBounces = 5;
    public LayerMask layertoCheck;
    CameraFollow m_cam;
    PlayerShooting ps;
    PlayerSplits splits;

    // Use this for initialization
    void Start()
    {
        m_cam = FindObjectOfType<CameraFollow>(); 
        lr = GetComponent<LineRenderer>();
        ps = FindObjectOfType<PlayerShooting>();
        splits = FindObjectOfType<PlayerSplits>(); 
    }

    // Update is called once per frame
    void Update()
    {
        NerdModeActivate();
    }

    void NerdModeActivate()
    {
        // whats max bullet distance? or max bounces?
        RaycastHit raycastHit;

        lr.positionCount = 1;
        lr.SetPosition(0, GunPos.position);
        direction = GunPos.forward;
        Ray ray = new Ray(GunPos.position, direction);


        for (int i = 0; i < maxBounces; i++)
        {
            Physics.Raycast(ray, out raycastHit, Mathf.Infinity, layertoCheck);

            Vector3 normal = raycastHit.normal;
            Vector3 deflect = Vector3.Reflect(ray.direction, normal);

            ray = new Ray(raycastHit.point, deflect);

            startPoint = raycastHit.point;

            lr.positionCount = lr.positionCount + 1;
            lr.SetPosition(lr.positionCount - 1, startPoint);

            //if (raycastHit.collider.tag == "Enemy")
            //{
            //    Debug.Log(raycastHit.collider.tag);
            //    if(raycastHit.transform.gameObject.GetComponent<MovingEnemy>().isTarget != false)               
            //    {
            //        Debug.Log("Camera should follow" + "Clone number : " + i);
            //        splits.SplitNumber(i); 
            //        //ps.ThingToFollow(lr.Get); 
            //    }
            //}

        }
    }
}
