﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float moveSpeed;
    Rigidbody rb;

    public bool cantMove;


    void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }
	void Update()
    {
        if (!cantMove)
        Move();
    }
   
    //Movement
    void Move()
    {
        Vector3 inputMovement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        transform.Translate(inputMovement* Time.deltaTime* moveSpeed);   
    }

}
