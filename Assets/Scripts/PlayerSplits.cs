﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to create split copies of the player at certain positions
/// </summary>

public class PlayerSplits : MonoBehaviour
{

    [SerializeField]
    private GameObject player;

    public Transform m_player;

    public List<GameObject> splits = new List<GameObject>();

    public bool canChangeRot;

    Transform gun;

    public int splitMax;
    public int splitCount;
    public int splitActive;

    private GameObject playerClone;

    PlayerMovement pm;

    PlayerShooting ps;

    public Transform gunPos;

    GameManager gm;

    GameObject splitToLookAt; 
    private void Awake()
    {
        pm = GetComponent<PlayerMovement>();
        gm = FindObjectOfType<GameManager>();
        ps = GetComponent<PlayerShooting>();
    }

    void Start()
    {
        splitCount = splitMax;
        //splits.Add(GameObject.FindGameObjectWithTag("Player"));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1) && splitCount != 0)
        {
            Split(m_player.transform.rotation);
            canChangeRot = true;
        }

        if (Input.GetKeyUp(KeyCode.Mouse1) && splitCount != 0)
        {
            //pm.cantMove = false;
            canChangeRot = false;
            splitCount--;
            splitActive++;
        }

        switch (canChangeRot)
        {
            case true:
                //pm.cantMove = true;
                playerClone.transform.rotation = m_player.transform.rotation;
                playerClone.transform.position = m_player.transform.position;
                break;
        }

        if (splitCount <= 0 && splitActive <= 0 && ps.bullets.Count <= 0 && !gm.hasWon)
        {
            gm.hasLost = true;
        }
    }

    public void Split(Quaternion rot)
    {
        playerClone = Instantiate(player, transform.position, rot) as GameObject;
        //GameObject gun = playerClone.GetComponentInChildren<GunGetter>().gameObject;

        //gun.transform.position = gunPos.transform.position;
        //gun.transform.rotation = gunPos.transform.rotation;
        //gun = playerClone.transform.GetChild(0).GetChild(0);
        //gun.transform.rotation = Quaternion.Euler(0, player.gameObject.transform.GetChild(0).GetChild(0).transform.rotation.y, 0);
        splits.Add(playerClone);
    }

    public void SplitNumber(int splitNumber)
    {
        splitToLookAt = splits[splitNumber].transform.gameObject; 
    }

    //public void DSplit()
    //{
    //    int i = splits.Count;
    //    Debug.Log(i);
    //    GameObject dsplit = splits[i-1].gameObject;
    //    Debug.Log(dsplit.name);
    //    Destroy(dsplit);
    //    splits.RemoveAt(i);
    //    splitActive--;
    //    splitCount++;
    //}
}
