﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake; 
/// <summary>
/// Used to shoot a bullet
/// </summary>

public class PlayerShooting : MonoBehaviour {

    [SerializeField]
    private GameObject bullet;
    PlayerSplits ps;

    //Manual Settings
    public int ind;
    private int indMax;

    //Delay Settings
    public bool delay;
    public float delayTimer;
    private float delayReset;

    public bool isOneActive;

    public List<GameObject> bullets = new List<GameObject>();

    AudioSource m_audio;
    public AudioClip shoot;
    public GameObject muzzleFlash; 

    void Awake()
    {
        m_audio = GetComponentInChildren<AudioSource>(); 
        ps = GetComponent<PlayerSplits>();

    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && ps.splitActive != 0)
        {
            //StartCoroutine("ShootBullet");
            if(isOneActive)
            ShootBullet();
        }

        for (int i = 0; i < bullets.Count; i++)
        {
            if (bullets[i].gameObject == null)
            {
                bullets.Remove(bullets[i]);
            }
        }

        if (ps.splitActive >= 1)
        {
            isOneActive = true;
        }
        //else if (ps.splitActive <= 1) 
        //{
        //    isOneActive = false; 
        //}
	}

    //IEnumerator ShootBullet()
    //{
    //    Transform bulletSpawnPos;
    //    for (int i = 0; i < ps.splits.Count; i++)
    //    {
    //        if (i == 0)
    //        {
    //            bulletSpawnPos = ps.splits[i].transform.GetChild(0).GetChild(0).GetChild(0); //Gets the child of the child of the child thats attached to this script (bullet spawn position)
    //            Instantiate(bullet, bulletSpawnPos.position, bulletSpawnPos.transform.rotation);
    //        }
    //        else
    //        {
    //            yield return new WaitForSeconds(delay);
    //            bulletSpawnPos = ps.splits[i].transform.GetChild(0).GetChild(0).GetChild(0); //Gets the child of the child of the child thats attached to this script (bullet spawn position)
    //            Instantiate(bullet, bulletSpawnPos.position, bulletSpawnPos.transform.rotation);
    //        }
    //    }
    //}

    void ShootBullet()
    {
        CameraShaker.Instance.ShakeOnce(1.5f, 1f, .1f, .3f);
        BulletMovement bm;
        TrajectoryCheck tc;
        Transform bulletSpawnPos;
        indMax = ps.splits.Count;

        m_audio.PlayOneShot(shoot, .5f);
        //if (ind == 0)
        //{
        //    bulletSpawnPos = ps.splits[ind].transform.GetChild(0).GetChild(0).GetChild(0); //Gets the child of the child of the child thats attached to this script (bullet spawn position)
        //    Instantiate(bullet, bulletSpawnPos.position, bulletSpawnPos.transform.rotation);
        //    ind++;
        //}
        //else
        //{
        GameObject dSplit;
        bulletSpawnPos = ps.splits[ind].transform.GetChild(0); //Gets the child of the child of the child thats attached to this script (bullet spawn position)
        tc = ps.splits[ind].GetComponent<TrajectoryCheck>();
        GameObject bulletClone = Instantiate(bullet, bulletSpawnPos.position, bulletSpawnPos.transform.rotation) as GameObject;
        Instantiate(muzzleFlash, bulletSpawnPos.position, bulletSpawnPos.transform.rotation); 
        bullets.Add(bulletClone) ;

        bm = bulletClone.GetComponent<BulletMovement>();

        for (int i = 0; i < tc.lr.positionCount; i++)
        {
            bm.wayPoints.Add(tc.lr.GetPosition(i));
        }

        dSplit = ps.splits[ind].gameObject;
        ps.splits.RemoveAt(ind);
        Destroy(dSplit);
        //ps.splitCount++;
        ps.splitActive--;

        //}
    }

    public void ThingToFollow(Transform dude)
    {

    }
}
