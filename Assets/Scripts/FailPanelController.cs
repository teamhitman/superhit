﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class FailPanelController : MonoBehaviour
{
	public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Menu()
    {
        SceneManager.LoadScene(0); 
    }
}
