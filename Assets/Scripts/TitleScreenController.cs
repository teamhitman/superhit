﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreenController : MonoBehaviour {

    public GameObject bullet;
    public Transform gunPos1;
    public Transform gunPos2;
    public Transform gunPos3;
    public Transform gunPos4;


    bool hasPLayed;
    public AudioClip impact; 
    AudioSource m_audio;

    SplineWalker walker;

    private void Start()
    {
        m_audio = GetComponent<AudioSource>(); 
        walker = FindObjectOfType<SplineWalker>(); 
    }

    public void OnClick_Start()
    {
        
        if(!hasPLayed)
        {
            walker.duration = 30;
            Instantiate(bullet, gunPos1.transform.position, gunPos1.transform.rotation);
            Instantiate(bullet, gunPos2.transform.position, gunPos2.transform.rotation);
            Instantiate(bullet, gunPos3.transform.position, gunPos3.transform.rotation);
            Instantiate(bullet, gunPos4.transform.position, gunPos4.transform.rotation);
            m_audio.PlayOneShot(impact, .3f); 
        }
        

       // Time.timeScale = .05f;
        //Application.LoadLevel(1);

    }

    public void OnClick_Quit()
    {
        Debug.Log("Quit");
        Application.Quit(); 

    }
}
