﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake; 

[RequireComponent(typeof(AudioSource))]
public class BulletDamage : MonoBehaviour
{
    public int maxBounceTimes;
    public int i;

    public Material[] materials;
    TrailRenderer lr;

    PlayerSplits ps;

    AudioSource m_audio;
    public AudioClip glassHit;
    public AudioClip enemyHit;
    public AudioClip wallHit;

    public int bouncedTimes; 
    int enemiesKilled;
    GameManager game; 

    LevelEndScore score;
    bool hasDestroyed;
    float timer;

    public GameObject scoreTxt;

    private void Start()
    {
        game = FindObjectOfType<GameManager>(); 
        bouncedTimes = 0;
        score = FindObjectOfType<LevelEndScore>(); 
        lr = GetComponent<TrailRenderer>();
        i = maxBounceTimes;
        m_audio = GetComponent<AudioSource>(); 
    }

    private void Update()
    {
        if (i == 3)
        {
            lr.material = materials[0];
        }
        if (i == 2)
            {
            lr.material = materials[1];
        }
        if (i == 1)
        {
            lr.material = materials[2];
        }

        if(!hasDestroyed)
        {           
            timer += Time.deltaTime;
            if(timer >= 5)
            {
                score.AddEnemyScore(enemiesKilled);
                
                Destroy(this.gameObject);
                hasDestroyed = true;
            }
        }    
    }

    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.CompareTag("Breakable Wall"))
        {
            other.gameObject.GetComponentInParent<BreakableWallController>().Damage(1, -transform.forward);
            //Destroy(this.gameObject);
        }

        if (other.gameObject.CompareTag("Solid Wall"))
        {
            bouncedTimes++; 
            i--;
            m_audio.PlayOneShot(wallHit, .05f);

            m_audio.pitch = .6f; 
           
        }

        if (other.gameObject.CompareTag("Glass"))
        {
            m_audio.PlayOneShot(glassHit, .05f);
            m_audio.pitch = 1f;
            other.gameObject.GetComponentInParent<BreakableWallController>().Damage(1, transform.forward);
            //i--;
          
        }
        if (other.gameObject.CompareTag("Target"))
        {
            other.gameObject.BroadcastMessage("Damage");
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("Civilian"))
        {
            other.gameObject.BroadcastMessage("Damage");
            
            //Destroy(this.gameObject);
        }
        if(other.gameObject.CompareTag("Enemy"))
        {
            CameraShaker.Instance.ShakeOnce(1.5f, 1f, .1f, .3f);
            other.gameObject.transform.gameObject.layer = 10;
            enemiesKilled++;
            m_audio.pitch = 1;
            m_audio.PlayOneShot(enemyHit, .1f);
            //get score number, instantate text mesh on enemy position and display score;


            GameObject scoreTxtClone = Instantiate(scoreTxt, this.transform.position, Quaternion.Euler(90, 0,0)) as GameObject;
            scoreTxtClone.transform.parent = other.gameObject.transform;
            scoreTxtClone.transform.position = new Vector3(other.collider.transform.position.x, other.collider.transform.position.y, other.collider.transform.position.z + 5);
            TextMesh scoretxt = scoreTxtClone.GetComponent<TextMesh>();
            if(enemiesKilled == 1)
            {
                scoretxt.text = ((1000)+ (bouncedTimes * 1000)).ToString();
            }
            if(enemiesKilled == 2)
            {
                scoretxt.text = ((1000)+(bouncedTimes * 1000 + 1000)).ToString();
            }
            if (enemiesKilled == 3)
            {
                scoretxt.text = ((1000) + (bouncedTimes * 1000 + 2000)).ToString();
            }


            MovingEnemy enemy = other.gameObject.GetComponent<MovingEnemy>();
           
            other.gameObject.BroadcastMessage("Damage");


            score.AddBounces(bouncedTimes);
            //Destroy(this.gameObject);
        }
        //if(game.hasLost || game.hasWon)
        //{
        //    score.AddEnemyScore(enemiesKilled);
        //    Destroy(this.gameObject); 
        //}
        if (i <= 0)
        {            
            score.AddEnemyScore(enemiesKilled); 
            Destroy(this.gameObject);

        }
    }
}
