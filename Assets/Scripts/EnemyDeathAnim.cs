﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeathAnim : MonoBehaviour
{
    Rigidbody rb;
    CapsuleCollider col;
    float Timer = 3f;
    bool started;

    void Start()
    {
        col = GetComponent<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = false;
        rb.AddForce(-transform.forward* 100 * Time.deltaTime, ForceMode.Impulse);
        started = true;
    }

    void Update()
    {
        if (started)
        {
            
            gameObject.layer = 10;
            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                rb.freezeRotation = true;
                
            }
        }
    }
}