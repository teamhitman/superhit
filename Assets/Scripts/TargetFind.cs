﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFind : MonoBehaviour {

    VisionConeController vc;
    MovingEnemy me;
    GameManager gm;
    CameraFollow m_cam;

	void Start ()
    {
        m_cam = FindObjectOfType<CameraFollow>(); 
        vc = GetComponent<VisionConeController>();
        me = GetComponent<MovingEnemy>();
        gm = FindObjectOfType<GameManager>();
    }
	
	void Update ()
    {
        CheckTargets();
	}
    
    void CheckTargets()
    {
        for (int i = 0; i < vc.visibleTargets.Count; i++)
        {
            switch (vc.visibleTargets[i].gameObject.layer)
            {
                case 11:
                    m_cam.FindEnemy(gameObject.transform); 
                    gm.hasLost = true;

                    break;
            }
            
            if (me.isTarget)
            {
                switch (vc.visibleTargets[i].gameObject.layer)
                {
                    case 10:
                        me.TargetRun();
                        break;

                    case 9:
                        me.TargetRun();
                        break;

                }
            }
        }
    }
}
