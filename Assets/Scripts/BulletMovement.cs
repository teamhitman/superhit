﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moves the bullet;
/// </summary>

public class BulletMovement : MonoBehaviour
{
    public float speed;
    Rigidbody rb;
    public Vector3 constantVel;

    public bool hitTarget;

    public List<Vector3> wayPoints = new List<Vector3>();

    GameObject target;
    int i;

    CameraFollow m_cam;
    bool hasSent; 
    //bullet needs to know what clone its coming from
    //bullet then gets the vector3 positions stored in clone lr
    //bullet then is moved between each position

    private void Start()
    {
        target = GameObject.Find("Target");
        rb = GetComponent<Rigidbody>();
        //i = 1;
        m_cam = FindObjectOfType<CameraFollow>();
    }

    private void Update()
    {
        //transform.position = Vector3.Lerp(transform.position, wayPoints[i], (1 * Time.deltaTime)); 
        transform.position = Vector3.MoveTowards(transform.position, wayPoints[i], (1 * Time.deltaTime) * speed);
        if (Vector3.Distance(transform.position, wayPoints[i]) < 0.1f)
        {          
            i++;
        }

        if (Vector3.Distance(wayPoints[i], target.transform.position) <= 1f)
        {
            Debug.Log("Bullet please follow");
            hitTarget = true;
            if (!hasSent)
            {
                m_cam.FollowBullet(gameObject.transform);
                hasSent = true; 
            }
            
        }

        if (hitTarget && Vector3.Distance(this.transform.position, wayPoints[i]) < 0.5f)
        {
            target.GetComponent<MovingEnemy>().Damage();
            Destroy(this.gameObject);
        }

        //rb.MovePosition(transform.position + transform.forward * speed);
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(collision.gameObject.tag != "Glass")
    //    {
    //        CalculateBounceRotation(collision);
    //    }
    //    
    //}
    //
    //private void CalculateBounceRotation(Collision collidedObject)
    //{
    //    Quaternion newRotation = transform.rotation;
    //
    //    Ray a = new Ray(transform.position, transform.forward);
    //    Ray b;
    //    RaycastHit hit;
    //
    //    if (Deflect(a, out b, out hit))
    //    {
    //        Debug.DrawLine(a.origin, hit.point);
    //        Debug.DrawLine(b.origin, b.origin + 3 * b.direction);
    //        Vector3 endPos = (b.origin + 3 * b.direction);
    //        //transform.position = a.origin; 
    //        transform.LookAt(endPos);
    //    }
    //    // return newRotation;
    //}
    //
    //
    //private bool Deflect(Ray ray, out Ray deflected, out RaycastHit hit)
    //{
    //
    //    if (Physics.Raycast(ray, out hit))
    //    {
    //        Vector3 normal = hit.normal;
    //        Vector3 deflect = Vector3.Reflect(ray.direction, normal);
    //
    //        deflected = new Ray(hit.point, deflect);
    //        return true;
    //    }
    //
    //    deflected = new Ray(Vector3.zero, Vector3.zero);
    //    return false;
    //}
}
