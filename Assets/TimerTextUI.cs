﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimerTextUI : MonoBehaviour {

    [HideInInspector]
    public float timeLeftTimer;
    GameManager game;
    Text timerText;
    Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>(); 
        timerText = GetComponent<Text>(); 
        game = FindObjectOfType<GameManager>(); 
    }
    private void Update()
    {
        timerText.text = timeLeftTimer.ToString("0.00"); 


        timeLeftTimer -= Time.deltaTime;
        if(timeLeftTimer <= 0)
        {
            game.hasLost = true;
            Destroy(this.gameObject);
        }

        if(timeLeftTimer <= 10)
        {
            StartCoroutine(WaitPing()); 
        }

        if(timeLeftTimer <= 5)
        {
            timerText.color = Color.red;
        }
    }

    IEnumerator WaitPing()
    {
        anim.SetBool("Play", true);
        yield return new WaitForSeconds(.3f);
        anim.SetBool("Play", false);
        
    }

    
}
