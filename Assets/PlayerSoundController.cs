﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerSoundController : MonoBehaviour {

    bool walking;
    float walkIndex; 
    AudioSource m_audio;
    public AudioClip footstep;

    void Start()
    {
        m_audio = GetComponent<AudioSource>(); 
    }

    public void FootStep(float isWalking)
    {
        if (gameObject.transform.parent.name == "Player")
        walkIndex = isWalking; 
    }

    void Update()
    {
        if(walkIndex == 1)
        {            
            walking = true;
        }

        if(walking)
        {
            m_audio.PlayOneShot(footstep, .5f);
            walkIndex = 0;
            walking = false;
        }
            
        
    }
}
