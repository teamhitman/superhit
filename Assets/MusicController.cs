﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {


    AudioSource m_audio;
    public AudioClip before;
    public AudioClip after;

    bool hasPlayed; 
    GameManager game;

    bool playedaudio;
    bool stopped;

    bool canPlay = true; 

    TimerSpawnUIController title;
    private void Start()
    {
        game = FindObjectOfType<GameManager>(); 
        title = FindObjectOfType<TimerSpawnUIController>(); 
        m_audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if(game.hasWon || game.hasLost)
        {
            m_audio.volume = Mathf.Lerp(m_audio.volume, 0, (1 * Time.deltaTime)); 
        }
        if(hasPlayed && playedaudio)
        {
            if(!stopped)
            {
                m_audio.Stop();
                stopped = true;
            }
            
        }
       
        if(stopped && canPlay)
        {
            m_audio.clip = after;
            m_audio.Play();
            canPlay = false;
        }
        if(title.hasSpawned)
        {
            if(!playedaudio)
            {
                
                playedaudio = true; 
            }
            
        }
        else
        {
            if(!hasPlayed)
            {
                //m_audio.PlayOneShot(before, .3f);
                m_audio.Play(); 
                //m_audio.loop = true;
               // m_audio.Play(before); 
                
                hasPlayed = true;
            }
            
        }
    }
}
