﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject[] enemies;
    public bool isStart;
    public bool hasWon;
    public bool hasLost;
    public bool Paused;

    public GameObject winPanel;
    public GameObject losePanel;
    public GameObject pausePanel;
    public GameObject hud;

    PlayerShooting shoot;
    PlayerMovement move;
    PlayerSplits split;
    public GunRotation gun;
    public GunRotation gun2;
    public int maxClones;

    bool hasDone; 

    public bool targetIsDead;

    private void Awake()
    {
       
        shoot = FindObjectOfType<PlayerShooting>();
        move = FindObjectOfType<PlayerMovement>();
        split = FindObjectOfType<PlayerSplits>();
        
    }

    private void LateUpdate()
    {
        if (losePanel.activeInHierarchy)
        {
            winPanel.SetActive(false);
            pausePanel.SetActive(false);
            hud.SetActive(false);
        }
        if (winPanel.activeInHierarchy)
        {
            losePanel.SetActive(false);
            pausePanel.SetActive(false);
            hud.SetActive(false);
        }
        if (pausePanel.activeInHierarchy)
        {
            losePanel.SetActive(false);
            winPanel.SetActive(false);
            hud.SetActive(false);
        }
        if (hud.activeInHierarchy)
        {
            losePanel.SetActive(false);
            winPanel.SetActive(false);
            pausePanel.SetActive(false);
        }
    }

    private void Update()
    {      
        

        if (Input.GetKeyDown(KeyCode.Escape) && !Paused)
        {
            Paused = true;
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && Paused)
        {
            
            Paused = false; 
        }
        if (!Paused)
        {           
            if(!hasDone)
            {
                Time.timeScale = 1;
                hasDone = true; 
            }
            
            EnablePlayer();
            pausePanel.SetActive(false);
            hud.SetActive(true);
        }
        if (Paused)
        {
            if(hasDone)
            {
                Time.timeScale = 0;
                hasDone = false; 
            }
                       
            pausePanel.SetActive(true);
            UnEnablePlayer();        
        }

        if (hasLost || targetIsDead)
        {
            if(!hasWon)
            {
                losePanel.SetActive(true);
                UnEnablePlayer();
            }
            
        }
        else
        {
            losePanel.SetActive(false);
        }

        if (hasWon)
        {
            if(!hasLost)
            {
                winPanel.SetActive(true);
                UnEnablePlayer();
            }
       
        }
        else
        {
            winPanel.SetActive(false);
            EnablePlayer(); 
        }

        SetSplits();

    }

    void EnablePlayer()
    {
        //shoot.enabled = true;
        move.enabled = true;
        split.enabled = true;
        //gun.enabled = true;
       // gun2.enabled = true;
        //m_cam.enabled = true; 
    }

    void UnEnablePlayer()
    {
        //shoot.enabled = false;
        move.enabled = false;
        split.enabled = false;
        //un.enabled = false;
        //gun2.enabled = false;     
    } 
    
    void SetSplits()
    {
        //split.splitMax = maxClones;
    }

}
