﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroy : MonoBehaviour {

    public float timer; 
	void Update()
    {
        Destroy(this.gameObject, timer); 
    }
}
