﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMove : MonoBehaviour
{

    float lifetime = 10;
    public float moveSpeed = 15;
    public bool isForward; 
    void Update()
    {
        if(isForward)
        {
            transform.Translate(transform.forward * moveSpeed);
        }
        else
        {
            transform.Translate(-transform.forward * moveSpeed);
        }
       
        Destroy(this.gameObject, 10);
    }
}