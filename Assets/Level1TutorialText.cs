﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
public class Level1TutorialText : MonoBehaviour {

    Text uiText;

    private void Start()
    {
        uiText = GetComponent<Text>(); 
    }
    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Mouse1))
        {
            uiText.text = "Left Click To Shoot From Clone";
        }
    }
}
