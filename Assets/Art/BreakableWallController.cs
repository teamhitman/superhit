﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableWallController : MonoBehaviour
{

    public int health;

    public Transform[] childs;

    public float breakForce;

    bool hasDone;

    Vector3 directionToBreak;

    MeshRenderer m_rend;

    public bool broken;

    void Start()
    {
        m_rend = GetComponent<MeshRenderer>(); 
        childs = new Transform[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            if (i == 0)
            {
                childs[i] = transform.GetChild(i);
                childs[i].gameObject.SetActive(true);
            }
            else
            {
                childs[i] = transform.GetChild(i);
                childs[i].gameObject.SetActive(false);
            }
        }
    }

    void Update()
    {
        if (health <= 0)
        {
            if (!hasDone)
            {
                Break();
                hasDone = true;
            }

        }
    }

    public void Damage(int dmg, Vector3 direction)
    {
        health -= dmg;        
        directionToBreak = direction;        
    }

    void Break()
    {
        broken = true; 
        for (int i = 0; i < childs.Length; i++)
        {
            if (i == 0)
            {
                childs[i].gameObject.SetActive(false);
            }
            else
            {

                childs[i].gameObject.SetActive(true);
                childs[i].gameObject.GetComponent<Rigidbody>().AddForce(directionToBreak * breakForce, ForceMode.Impulse);

                //For later, this is so we can lerp its scale down
                var locScale = childs[i].gameObject.GetComponent<Transform>().localScale;
                locScale = locScale / 2;              

                //childs[i].gameObject.GetComponent<Transform>().localScale = locScale;
                hasDone = true;
            }
        }
    }

    
}
