﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StartLevelUIController : MonoBehaviour
{

    public int textAmount;

    public string[] textToShow;
    int i;
    public GameObject textToSpawn;

    float waitTimer = .5f;
    float initialWaitTimer;
    public GameObject m_canvas;
    public Transform spawnPos;

    GameObject flasher;
    AudioSource m_audio;

    ObjectDestroy destroyer;

    public AudioClip textNoise; 
    void Start()
    {
        m_audio = GetComponent<AudioSource>(); 
        i = 0;
        initialWaitTimer = waitTimer;
        flasher = GameObject.Find("Flasher");
        destroyer = flasher.GetComponent<ObjectDestroy>();
        destroyer.timer = textAmount * .5f;
        Spawn();
    }

    void Update()
    {
        waitTimer -= Time.deltaTime;
        if (waitTimer <= 0)
        {            
            Spawn();
            
            
        }
    }


    void Spawn()
    {
        if (i < textAmount)
        {
            m_audio.PlayOneShot(textNoise, .5f);
            GameObject text1 = Instantiate(textToSpawn, spawnPos.transform.position, Quaternion.identity) as GameObject;
            text1.transform.SetParent(m_canvas.transform);     
            Text m_text = text1.GetComponent<Text>();
            m_text.text = textToShow[i];
            i += 1;
            waitTimer = initialWaitTimer;
        }
    }


}

