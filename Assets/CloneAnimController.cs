﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneAnimController : MonoBehaviour {

    GameObject player;
    GameObject playerAnims;

    Animator playerAnimator;
    Animator m_anim;
    private void Start()
    {
        m_anim = GetComponent<Animator>(); 
        player = GameObject.Find("Player");
        playerAnims = player.transform.GetChild(0).gameObject;
        playerAnimator = playerAnims.GetComponent<Animator>(); 
    }

    private void Update()
    {
        //AnimatorStateInfo animationState = playerAnimator.GetCurrentAnimatorStateInfo(0);
        //AnimatorClipInfo[] myAnimatorClip = playerAnimator.GetCurrentAnimatorClipInfo(0);

        //float myTime = myAnimatorClip[0].clip.length * animationState.normalizedTime;

        //AnimatorStateInfo m_AnimationState = m_anim.GetCurrentAnimatorStateInfo(0);
        //m_AnimationState = animationState; 

        float move = Input.GetAxis("Horizontal");
        move += Input.GetAxis("Vertical");

        if(move == 0)
        {
            m_anim.SetBool("isIdle", true);
        }
        else
        {
            m_anim.SetBool("isIdle", false); 
        }

        m_anim.SetFloat("Move", move); 



        //Animation animToPlay = playerAnimator.GetCurrentAnimatorClipInfo(1);

    }
}
