﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameTrigger : MonoBehaviour
{
    GameManager game;
    // Use this for initialization
    void Start()
    {
        game = FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        MovingEnemy enemy = other.gameObject.GetComponent<MovingEnemy>();
        if (enemy.isTarget)
          {
            game.hasLost = true;
        }
    }
}
